#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Module ParseTrees
##########################################################################################
def gettrees(path):
    """
    Open a file with trees written in Phyllip format.
    Trees must end in semi-colon.
    All invisible characters will be removes.
    Returns a list os trees.
    """
    import re
    handle=open(path,"rU")
    trees=handle.read()
    handle.close()
    trees=re.sub("\s","",trees)
    trees=trees.split(";")
    return trees
##########################################################################################
def getclades(tree):
    """
    Takes a single tree in parenthetical notation.
    Returns a list of all its clades sorted by terminal name.
    """
    import re
    clades=[]
    while "(" in tree:
        inner_clades=re.compile("\([^()]+\)").findall(tree)
        for fnd in inner_clades:
            rplc=fnd
            rplc=rplc.replace("(","[")
            rplc=rplc.replace(")","]")
            clades+=[sorted(re.compile("[^\[\],]+").findall(rplc))]
            tree=tree.replace(fnd,rplc)
    clades=sorted(clades)
    return clades
##########################################################################################
def parenthetic(clades):
    """
    Gets clades as a list of lists.
    Returns a tree in Phyllip format.
    """
    clades=sorted(clades,key=len) # sort clades by size
    tree=clades[-1] # start from the list of all terminals
    for i in clades:
        not_in_tree=[]
        A=[]
        B=[]
        for j in tree:
            if(set(flatlist(j))&set(i)):
                A.append(j)
            else:
                B.append(j)
        tree=[A]+B
    tree=str(tree)
    dic={" ":"","'":"","[":"(","]":")"}
    for key in dic:
       tree=tree.replace(key,dic[key])
    return tree
##########################################################################################
def flatlist(list):
    """
    Returns a flat list of terminals given a list of lists of terminals.
    Assumes that terminal names do not have spaces nor special characters.
    """
    import re
    list=str(list)
    result=re.compile("[^,\[\]\'\"\s]+").findall(list)
    return result
##########################################################################################
def gettreefromnexus(file,tree_file,verbose):
    """
    Extract topology, number of terminals and labels from the NEXUS tree file.
    """
    import sys,re
    translate=re.compile("begin trees;\s+translate\s+([^;]+);",re.I|re.S).findall(file)
    if(not translate):
        sys.stderr.write(">ERROR: did not found translate section in %s\n"%(tree_file))
        exit()
    TRANSLATE={}
    codes=re.compile("^\s*(\d+)\s+.+",re.M).findall(translate[0])
    names=re.compile("^\s*\d+\s+([^,;]+)[,;]*",re.M).findall(translate[0])
    if(len(codes)!=len(names)):
        sys.stderr.write(">ERROR: something went wrong while parsing the translate section in %s\n"%(tree_file))
        exit()
    for i in range(0,len(codes)):
        TRANSLATE[codes[i]]=names[i]
    names=[]
    last=codes[-1]
    size=len(codes)
    codes=[]
    if(verbose):sys.stdout.write("-- Found %d terminals\n"%(size))
    try:
        tree=re.compile("%s[^;]*;.+?(tree.+?)\s*end;"%(TRANSLATE[last]),re.I|re.S).findall(file)[0].split("\n")[0]
    except:
        sys.stderr.write(">ERROR: no tree found in in %s\n"%(tree_file))
        exit()
    else:
        tree=re.compile("^tree[^\(]+BEST[^\(]+(.+)[\*;]",re.IGNORECASE).findall(tree)[0] # get the first tree
    tree=re.sub(":[^,\)]+","",tree) # remove branch lengths
    return tree,size,TRANSLATE

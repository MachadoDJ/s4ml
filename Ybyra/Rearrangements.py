#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Module Rearrangements
from ParseTrees import flatlist
##########################################################################################
def nested(clades):
    """
    Gets clades as a list of lists.
    Returns clades in nested lists.
    """
    clades=sorted(clades,key=len) # sort clades by size
    tree=clades[-1] # start from the list of all terminals
    for i in clades:
        not_in_tree=[]
        A=[]
        B=[]
        for j in tree:
           if(set(flatlist(j))&set(i)):
               A.append(j)
           else:
               B.append(j)
        tree=[A]+B
    return tree
##########################################################################################
def nni(c,C):
    import itertools
    """
    Receives a clade as a list of terminals and a tree as a list of clades.
    Returns all possible Nearest-Neighbor-Interchange (NNI) rearrangements.
    """
    trees=[]
    C=[i for i in C if i!=c]
    C=sorted(C,key=len)
    X=[]
    for i in C[:len(C)-1]:
        if(set(i)&set(c))and(len(set(i)-set(c))>=1):
            X=[i]
            break
    if(X):
        """
        This deals with all nodes that are at least one node away from the root.
        """
        for i in C[:len(C)-1]:
            if(set(i)&set(X[0]))and(len(i)<len(X[0])):
                X+=[i]
        X=nested(X)[0]
        """
        In a binary tree, we expect X to have 3 itens, resulting in tree possible
        combinations of 2 itens without repetitions.
        """
    else:
        """
        Nodes more close to the root need to be dealt with differently.
        Remember the tree here is just several lists of lists.
        """
        X=[c]
        Y=[]
        for i in C[:len(C)-1]:
            if(set(i)&set(c)):
                X+=[i]
            else:
                Y+=[i]
        X=nested(X)[0]+nested(Y)
    """
    We use itertools.combinations to make rearrangements.
    """
    original=sorted([sorted(x) for x in C+[c]])
    for i in itertools.combinations(X,2):
        i=flatlist(list(i))
        j=sorted([sorted(x) for x in C+[i]])
        if(j!=original):
            trees+=[j]
    return trees

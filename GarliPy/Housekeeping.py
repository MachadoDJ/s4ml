#!/usr/bin/env python3
#-*- coding: utf-8 -*-

def clean(output,path):
    """
    Remove some files produced during Garli runs
    """
    import os
    os.chdir(path)
    print(">Selected files:")
    files2del=["*.best.all.tre","*.best.tre","*.boot.conf","*.boot.tre","*.conf","*.constraint.tre","*.log*.log","*.screen.log","*sumtrees.log","*_aLRT_*","*_opt.*"]
    for i in range(0,len(files2del)):
        print("-- %s%s"%(output,files2del[i]))
    try:
    	user_choice=raw_input(">Delete selected files? [y/n]: ")
    except:
    	user_choice=input(">Delete selected files? [y/n]: ")
    if(user_choice[0].lower()=="y"):
        from glob import glob
        for i in files2del:
            for file in glob("%s%s"%(output,i)):
                try:
                    os.remove(file)
                except:
                    print("-- Could not remove %s"%(file))
                else:
                    print("-- Deleted %s"%(file))
    exit()

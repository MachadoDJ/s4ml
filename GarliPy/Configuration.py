#!/usr/bin/env python3
#-*- coding: utf-8 -*-

def getconstraints(tree,size,TRANSLATE,selected_clades,verbose):
    """
    Produces contraints for all the internal nodes in the extracted tree.
    """
    import sys,re
    if(verbose):
        sys.stdout.write(">Extracting clades\n")
        sys.stdout.flush()
    proceed=True
    NODES={}
    CONSTRAINTS={}
    count=0
    while proceed==True:
        clades=re.compile("(\([^\()]+\))").findall(tree)
        for i in clades:
            j=i.replace("(","[")
            j=j.replace(")","]")
            tree=tree.replace(i,j)
            terminals=re.compile("([^,;\[\]]+)").findall(j)
            if size-len(terminals)<=1:
                break
            count+=1
            NODES[count]=terminals
            constraint="-("
            for x in TRANSLATE:
                if terminals.count(x)==0:constraint+="%s,"%(TRANSLATE[x])
            constraint+="("
            translated_terminals=sorted([TRANSLATE[x] for x in terminals])
            if(translated_terminals in selected_clades)or(len(selected_clades)==0):
                for y in translated_terminals:
                    constraint+="%s,"%(y)
                constraint=constraint[:len(constraint)-1]+"))"
                CONSTRAINTS[count]=constraint
        if tree.count("(")==0:proceed=False
    if(verbose):
        sys.stdout.write("-- Found %d internal nodes\n"%(count))
    return CONSTRAINTS,NODES,count

##########################################################################################

def summarize(tree,size,TRANSLATE,CONSTRAINTS,NODES,count,output,tree_file,verbose):
    import sys,re,os
    from math import exp
    """
    Parse trees and calculates LLD.
    """
    if(verbose):
        sys.stdout.write(">Parsing trees and calculating LLD\n")
        sys.stdout.flush()
    if(os.path.isfile(tree_file)==False):
        sys.stderr.write(">ERROR: could not find %s\n"%(tree_file))
        sys.stderr.flush()
        exit()
    else:
        handle=open(tree_file,"r")
    file=handle.read()
    handle.close()
    bestScore=re.compile("tree.*BEST.*\[!GarliScore\s*([^\]]+?)\s*\]",re.IGNORECASE).findall(file)[0]
    SUPPORT={}
    for i in NODES:
        file="%s_%d_results.best.tre"%(output,i)
        if(os.path.isfile(file)==False):
            score=0
            SUPPORT[i]=0
        else:
            handle=open(file,"r")
            file=handle.read()
            handle.close()
            score=re.compile("tree.*BEST.*\[!GarliScore\s*([^\]]+?)\s*\]",re.IGNORECASE).findall(file)[0]
            SUPPORT[i]=float(bestScore)-float(score)
    tsv="NODE NUMBER\tLOG-LIKELIHOOD DIFFERENCES (LLD)\tLikelihood Ratio (LR)\tTERMINALS\n"
    for i in SUPPORT:
        terminals=[]
        for j in NODES[i]:terminals+=[TRANSLATE[j]]
        tsv+="%d\t%s\t%f\t%s\n"%(i,SUPPORT[i],(1.0-exp(-1.0*float(SUPPORT[i])) ),terminals)
        if(verbose):
            sys.stdout.write("-- Node number %d : LLD = %s : LR = %f\n"%(i,SUPPORT[i], (1.0-exp(float(-1.0*SUPPORT[i])))   ))
            sys.stdout.flush()
    handle=open("%s_support.tsv"%(output),"w")
    handle.write(tsv)
    handle.close()
    if(verbose):
        sys.stdout.write("--> Support in tab separated values wrote into %s_support.tsv\n"%(output))
        sys.stdout.flush()
    support(tree,SUPPORT,NODES,TRANSLATE,output,verbose)
    return

##########################################################################################

def support(tree,SUPPORT,NODES,TRANSLATE,output,verbose):
    """
    Print tree with node numbers and support values.
    """
    import sys,re
    from math import exp
    if(verbose):
        sys.stdout.write(">Writing support tree\n")
        sys.stdout.flush()
    insert=True
    count=0
    while insert==True:
        clades=re.compile("(\([^\()]+\))").findall(tree)
        for i in clades:
            count+=1
            if count>len(SUPPORT):
                insert=False
                break
            j=i.replace("(","[")
            #j=j.replace(")","]%d:%f"%(count,SUPPORT[count]))
            #j=j.replace(")","]%d:%f"%(count,exp(float(SUPPORT[count]))))
            j=j.replace(")","]%d:%f"%(count,(1.0-exp(-1.0*float(SUPPORT[count])))))
            tree=tree.replace(i,j)
    tree=tree.replace("[","(")
    tree=tree.replace("]",")")
    nexus="#NEXUS\nbegin trees;\ntranslate\n"
    for i in TRANSLATE:
        nexus+=" %s %s,\n"%(i,TRANSLATE[i])
    nexus=nexus[:len(nexus)-2]+";\ntree support4ml = %s;\nend;"%(tree)
    handle=open("%s_support.tre"%(output),"w")
    handle.write(nexus)
    handle.close()
    if(verbose):
        sys.stdout.write("--> Tree with node numbers and support values (LR) wrote into %s_support.tre\n"%(output))
        sys.stdout.flush()
    return

##########################################################################################

def editConf(i,opt,args_bootstrapreps,args_configuration,args_output,args_searchreps,args_stopgen,args_stoptime):
    """
    Prepare Garli's configuration file.
    """
    import os,sys,re
    if(os.path.isfile(args_configuration)==False):
        sys.stderr.write(">ERROR: could not find %s\n"%(args_configuration))
        sys.stderr.flush()
        exit()
    else:
        handle=open(args_configuration,"r")
        conf=handle.read()
        handle.close()
    # searchreps
    if(args_searchreps):
        try:
            searchreps=re.compile("(searchreps[\s=]+[^\n\r]+)").findall(conf)[0]
            conf=re.sub(searchreps,"searchreps = %d"%(args_searchreps),conf,flags=re.I)
        except:
            conf=re.sub("\[GENERAL\]","[general]\nsearchreps = %d"%(args_searchreps),conf,flags=re.I)
    # stopgen
    if(args_stopgen):
        try:
            stopgen=re.compile("(stopgen[\s=]+[^\n\r]+)").findall(conf)[0]
            conf=re.sub(stopgen,"stopgen = %d"%(args_stopgen),conf,flags=re.I)
        except:
            conf=re.sub("\[MASTER\]","[MASTER]\nstopgen = %d"%(args_stopgen),conf,flags=re.I)
    # stoptime
    if(args_stoptime):
        try:
            stoptime=re.compile("(stoptime[\s=]+[^\n\r]+)").findall(conf)[0]
            conf=re.sub(stoptime,"stoptime = %d"%(args_stoptime),conf,flags=re.I)
        except:
            conf=re.sub("\[MASTER\]","[MASTER]\stoptime = %d"%(args_stoptime),conf,flags=re.I)
    if(opt==0):
        # ofprefix
        try:
            ofprefix=re.compile("(ofprefix[\s=]+[^\n\r]+)",re.I).findall(conf)[0]
            conf=re.sub(ofprefix,"ofprefix = %s_%s_results"%(args_output,i),conf,flags=re.I)
        except:
            conf=re.sub("\[GENERAL\]","[general]\nofprefix = %s_%s_results"%(args_output,i),conf,flags=re.I)
        # constraintfile
        try:
            constraintfile=re.compile("(constraintfile[\s=]+[^\n\r]+)",re.I).findall(conf)[0]
            conf=re.sub(constraintfile,"constraintfile = %s_%s.constraint.tre"%(args_output,i),conf,flags=re.I)
        except:
            conf=re.sub("\[GENERAL\]","[general]\nconstraintfile = %s_%s.constraint.tre"%(args_output,i),conf,flags=re.I)
        # bootstrapreps
        try:
            bootstrapreps=re.compile("(bootstrapreps[\s=]+[^\n\r]+)").findall(conf)[0]
            conf=re.sub(bootstrapreps,"bootstrapreps = 0",conf,flags=re.I)
        except:
            conf=re.sub("\[MASTER\]","[master]\nbootstrapreps = 0",conf,flags=re.I)
    elif(opt==1):
        # ofprefix
        try:
            ofprefix=re.compile("(ofprefix[\s=]+[^\n\r]+)",re.I).findall(conf)[0]
            conf=re.sub(ofprefix,"ofprefix = %s_%s_bootstrap"%(args_output,i),conf,flags=re.I)
        except:
            conf=re.sub("\[GENERAL\]","[general]\nofprefix = %s_%s_bootstrap"%(args_output,i),conf,flags=re.I)
        # constraintfile
        try:
            constraintfile=re.compile("(constraintfile[\s=]+[^\n\r]+)",re.I).findall(conf)[0]
            conf=re.sub(constraintfile,"constraintfile = none",conf,flags=re.I)
        except:
            conf=re.sub("\[GENERAL\]","[general]\nconstraintfile = none",conf,flags=re.I)
        # bootstrapreps
        try:
            bootstrapreps=re.compile("(bootstrapreps[\s=]+[^\n\r]+)").findall(conf)[0]
            conf=re.sub(bootstrapreps,"bootstrapreps = %d"%(args_bootstrapreps),conf,flags=re.I)
        except:
            conf=re.sub("\[MASTER\]","[master]\nbootstrapreps = %d"%(args_bootstrapreps),conf,flags=re.I)
    return conf

##########################################################################################

def optimizeinputonly(config,tree):
    import re
    handle=open(config,"rU")
    conf=handle.read()
    handle.close()
    name=str(re.compile("^([^_]+)").findall(tree)[0])
    number=int(re.compile("_aLRT_(\d+)").findall(tree)[0])
    try:
        ofprefix=re.compile("(ofprefix[\s=]+[^\n\r]+)",re.I).findall(conf)[0]
        conf=re.sub(ofprefix,"ofprefix = %s_%s_opt"%(name,number),conf,flags=re.I)
    except:
        conf=re.sub("\[GENERAL\]","[general]\nofprefix = %s_%s_opt"%(name,number),conf,flags=re.I)
    try:
        streefname=re.compile("(streefname[\s=]+[^\n\r]+)",re.I).findall(conf)[0]
        conf=re.sub(streefname,"streefname = %s"%(tree),conf,flags=re.I)
    except:
        conf=re.sub("\[GENERAL\]","[general]\nstreefname = %s"%(tree),conf,flags=re.I)
    try:
        optimizeinputonly=re.compile("(optimizeinputonly[\s=]+[^\n\r]+)",re.I).findall(conf)[0]
        conf=re.sub(optimizeinputonly,"optimizeinputonly = 1",conf,flags=re.I)
    except:
        conf=re.sub("\[GENERAL\]","[general]\noptimizeinputonly = 1",conf,flags=re.I)
    return conf

#!/usr/bin/env python3
#-*- coding: utf-8 -*-

#support4ml.py

"""
version 2016.09.27

LICENSE:
* Copyright (C) 2016 - Denis Jacob Machado
* GNU General Public License version 3.0

AUTHOR:
* Denis Jacob Machado, Ph.D.
* Email: machadodj [at] alumni [dot] usp [dot] br

TREE INPUT:
* Trees in Newick format (best tree from Garli)
* Only the first tree of the file will be read
* Node labels and branch lengths will be ignored if present

CONFIGURATION INPUT:
* Template configuration file
* Fields that will be modified: ofprefix and constraintfile
* Edit other fields as wanted

OUTPUT:
* Reverse-constraint trees for Garli
* Reverse-constraint configuration files for Garli

See README.md for further details.
"""
##########################################################################################

try:
	import argparse,re,os,subprocess,sys
except:
	sys.stderr.write(">ERROR: could import general libraries\n")
	exit()
try:
	from multiprocessing import Process,Queue,cpu_count
except:
	sys.stderr.write(">ERROR: Could not import Process and Queue from multiprocessing\n")
	exit()
try:
	from Ybyra import ParseTrees
	from GarliPy import Housekeeping,Configuration
except:
	sys.stderr.write(">ERROR: Could not import specific modules\n")
	exit()

##########################################################################################

path=os.getcwd() # PATH TO WORKING DIRECTORY

##########################################################################################

parser=argparse.ArgumentParser()
parser.add_argument("-a","--alrt",help="Performs the Approximate Likelihood Ratio Test",action="store_true")
parser.add_argument("-b","--bootstrapreps",help="The number of bootstrap replicas to perform per cpu (0 to infinity)",type=int)
parser.add_argument("-c","--configuration",help="Configuration file used during your tree search in Garli (default = garli.conf)",type=str,default="garli.conf")
parser.add_argument("-g","--garli",help="Path to your serial Garli executable (default = garli)",type=str,default="garli")
parser.add_argument("-o","--output",help="Preffix of the output constraints file (default = OUTPUT)",type=str,default="OUTPUT")
parser.add_argument("-p","--processes",help="maximum processes to run simultaneously (default = max. number of cpus)",type=int,default=cpu_count())
parser.add_argument("-s","--sumtrees",help="Path to sumtrees.py (default = sumtrees.py)",type=str,default="sumtrees.py")
parser.add_argument("-t","--tree",help="A tree file in Newich format (default = search.best.tre)",type=str,default="search.best.tre")
parser.add_argument("-v","--verbose",help="Increase output verbosity",action="store_true")
parser.add_argument("-V","--version",help="Print version number and quit",action="store_true",default=False)
parser.add_argument("--clean",help="Remove least important output files",action="store_true",default=False)
parser.add_argument("--clades",help="Constraint file with selected clades",type=str)
parser.add_argument("--searchreps",help="The number of independent search replicates (1 to infinity)",type=int)
parser.add_argument("--stopgen",help="The maximum number of generations to run",type=int)
parser.add_argument("--stoptime",help="The maximum number of seconds for the run to continue",type=int)

args=parser.parse_args()

# Version

if(args.version):
	"""
	Print version and exit.
	"""
	sys.stdout.write("version 2016.09.27\n")
	exit()

# Housekeeping

if(args.clean):
	Housekeeping.clean(args.output,path)

# Selected clades

selected_clades=[]
if(args.clades):
	"""
	Get list of selected clades if the user provided any.
	"""
	try:
		handle=open(args.clades,"rU")
	except:
		sys.stdout.write(">Error: could not read %s"%(args.clade))
		exit()
	for line in handle.readlines():
		line=re.sub("[\s\n();]","",line)
		if(line):
			clade=sorted(line.split(","))
			selected_clades.append(clade)
	handle.close()

##########################################################################################

def configure():
	"""
	Take the first tree from the input nexus file. Extract taxon names and internal nodes.
	Create constraints.
	"""
	if(args.verbose):
		sys.stdout.write(">Reading input tree from Garli %s\n"%(args.tree))
		sys.stdout.flush()
	if(os.path.isfile(args.tree)==False):
		sys.stderr.write(">ERROR: could not find %s\n"%(args.tree))
		exit()
	else:
		handle=open(args.tree,"r")
	file=handle.read()
	handle.close()
	tree,size,TRANSLATE=ParseTrees.gettreefromnexus(file,args.tree,args.verbose)
	CONSTRAINTS,NODES,count=Configuration.getconstraints(tree,size,TRANSLATE,selected_clades,args.verbose)
	"""
	Edit configuration file.
	"""
	if(args.verbose):
		sys.stdout.write("-- Printing constraint trees and configuration\n")
		sys.stdout.flush()
	"""
	Print constraint trees and the respective configuration files.
	"""
	accumulateProcesses=[]
	for i in CONSTRAINTS:
		accumulateProcesses+=[i]
		out=open("%s_%s.constraint.tre"%(args.output,i),"w")
		out.write(CONSTRAINTS[i])
		out.close()
		conf=Configuration.editConf(i,0,args.bootstrapreps,args.configuration,args.output,args.searchreps,args.stopgen,args.stoptime)
		out=open("%s_%s.conf"%(args.output,i),"w")
		out.write(conf)
		out.close()
		"""
		Execute parallel searches once enough jobs have been accumulated.
		"""
		if(len(accumulateProcesses)==args.processes):
			createJobs(accumulateProcesses)
			accumulateProcesses=[]
	if(accumulateProcesses):
		createJobs(accumulateProcesses)
		accumulateProcesses=[]
	Configuration.summarize(tree,size,TRANSLATE,CONSTRAINTS,NODES,count,args.output,args.tree,args.verbose)
	return

##########################################################################################

def createJobs(accumulateProcesses):
	"""
	Creates and executes new parallel jobs.
	"""
	jobs=[]
	for i in accumulateProcesses:
		sys.stdout.write("--> Constraint search: node %d\n"%(i))
		j=Process(target=runGarli,args=(i,)) # create new process
		jobs.append(j) # add new process to the list
		j.start() # start new process
	for j in jobs:
		j.join() # wait processes to finish
	return

#----------------------------------------------------------------------------------------#

def runGarli(i):
	commandLine="%s %s/%s_%d.conf > /dev/null 2>&1"%(args.garli,path,args.output,i)
	"""
	The line above may need to be modified to for non-unixes systems or systems without
	the /dev/null. You can remove ' > /dev/null 2>&1' but this will increase output
	verbosity.
	"""
	subprocess.check_call(commandLine,shell=True)
	return

##########################################################################################

def bootstrap():
	"""
	Run bootstrap. Each processor will run Garli with the number of bootstrap search
	replicates provided.
	"""
	if(args.verbose):
		sys.stdout.write(">Executing bootstrap replicates (%d reps. per process, %d processes)\n"%(args.bootstrapreps,args.processes))
		sys.stdout.flush()
	accumulateProcesses=[]
	for i in range(1,args.processes+1):
		accumulateProcesses+=[i]
		conf=Configuration.editConf(i,1,args.bootstrapreps,args.configuration,args.output,args.searchreps,args.stopgen,args.stoptime)
		out=open("%s_%d.boot.conf"%(args.output,i),"w")
		out.write(conf)
		out.close()
	createBootJobs(accumulateProcesses)
	sumt(accumulateProcesses)
	parset()
	return

#----------------------------------------------------------------------------------------#

def createBootJobs(accumulateProcesses):
	"""
	Creates and executes new bootstrap analyses.
	"""
	jobs=[]
	for i in accumulateProcesses:
		sys.stdout.write("-- proc. %d\n"%(i))
		sys.stdout.flush()
		j=Process(target=runBoot,args=(i,)) # create new process
		jobs.append(j) # add new process to the list
		j.start() # start new process
	for j in jobs:
		j.join() # wait processes to finish
	return

#----------------------------------------------------------------------------------------#

def runBoot(i):
	commandline="%s %s/%s_%d.boot.conf > /dev/null 2>&1"%(args.garli,path,args.output,i)
	"""
	The line above may need to be modified to for non-unixes systems or systems without
	the /dev/null. You can remove ' > /dev/null 2>&1' but this will increase output
	verbosity.
	"""
	subprocess.check_call(commandline,shell=True)
	return

#----------------------------------------------------------------------------------------#

def sumt(accumulateProcesses):
	if(args.verbose):
		sys.stdout.write(">Summaryzing results\n")
		sys.stdout.flush()
	target="%s_support.tre"%(args.output)
	output="%s_combined.tre"%(args.output)
	commandline="%s --no-taxa-block --percentages -t %s -o %s"%(args.sumtrees,target,output)
	for i in accumulateProcesses:
		commandline+=" %s_%d_bootstrap.boot.tre"%(args.output,i)
	commandline+=" > %s_sumtrees.log 2>&1"%(args.output)
	subprocess.check_call(commandline,shell=True)
	if(args.verbose):
		sys.stdout.write("--> Tree with LLD and bootstrap wrote into %s\n"%(output))
		sys.stdout.flush()
	return

#----------------------------------------------------------------------------------------#

def parset():
	filename="%s_combined.tre"%(args.output)
	handle=open(filename,"rU")
	tree=handle.read()
	handle.close()
	tree=re.compile("TREE support4ml = [^(]+(.+)",re.I).findall(tree)[0]
	tree=re.sub("\[[^]]+\]","",tree)
	handle=open("%s_combined.phy"%(args.output),"w")
	handle.write(tree)
	handle.close()
	if(args.verbose):
		sys.stdout.write("--> Output tree in Phyllip format wrote into %s_combined.phy\n"%(args.output))
		sys.stdout.flush()
	table="Clade\tBoostrap\tLLD\tTerminals\n"
	count=0
	while "(" in tree:
		clades=re.compile("\([^\(\)]+\)[^,;()]+").findall(tree)
		for clade in clades:
			count+=1
			code="Clade_%d"%(count)
			try:
				bootstrap=re.compile("\)([^:,;\(\)]+)").findall(clade)[0]
			except:
				bootstrap="None"
			try:
				support=re.compile(":([^,;\(\)]+)").findall(clade)[0]
			except:
				support="None"
			terminals=re.compile("\(.+?\)").findall(clade)[0]
			table+="%s\t%s\t%s\t%s\n"%(code,bootstrap,support,terminals)
			tree=tree.replace(clade,code)
	handle=open("%s_combined.tsv"%(args.output),"w")
	handle.write(table)
	handle.close()
	if(args.verbose):
		sys.stdout.write("--> Table in tab separated values wrote into %s_combined.tsv\n"%(args.output))
		sys.stdout.flush()
	return

##########################################################################################

def main_alrt():
	if(args.verbose):
		sys.stdout.write(">Reading input tree from Garli %s\n"%(args.tree))
		sys.stdout.flush()
	if(os.path.isfile(args.tree)==False):
		sys.stderr.write(">ERROR: could not find %s\n"%(args.tree))
		exit()
	else:
		handle=open(args.tree,"r")
	file=handle.read()
	handle.close()
	scores=[float(x) for x in re.compile("\[!\s*garliscore\s*([^\]]+)",re.I).findall(file)]
	best=max(scores)
	tree,size,TRANSLATE=ParseTrees.gettreefromnexus(file,args.tree,args.verbose)
	clades=ParseTrees.getclades(tree)
	from Ybyra import Rearrangements
	if(args.verbose):
		sys.stdout.write("> Calculating aLRT\n")
		sys.stdout.flush()
	count_clades=0
	clade_dic={}
	for clade in [clade for clade in sorted(clades,key=len) if len(clade)<(len(TRANSLATE)-1)]:
		count_clades+=1
		clade_dic[count_clades]=clade
		if(args.verbose):
			sys.stdout.write("-- Clade %d of %d\n"%(count_clades,len(clades)-1))
			sys.stdout.flush()
		all_nni_trees=Rearrangements.nni(clade,clades)
		trees2print=[]
		for nni_tree in all_nni_trees:
			trees2print+=[ParseTrees.parenthetic(nni_tree)[1:-1]]
		filename=print_nni_tree(clade,trees2print,TRANSLATE,count_clades)
		conf=Configuration.optimizeinputonly(args.configuration,filename)
		confname="%s_aLRT_%d.conf"%(args.output,count_clades)
		handle=open(confname,"w")
		handle.write(conf)
		handle.close()
		runConf(confname)
	alrt_dic=calcaLRT(clade_dic,best)
	tsv,nexus=reportaLRT(alrt_dic,clade_dic,clades,TRANSLATE)
	handle=open("%s_aLTR.tsv"%(args.output),"w")
	handle.write(tsv)
	handle.close()
	handle=open("%s_aLTR.tre"%(args.output),"w")
	handle.write(nexus)
	handle.close()
	if(args.verbose):
		sys.stdout.write("> aLRT analysis has successfully concluded\n-- Support values (tab-separated values): %s_aLRT.tsv\n-- Tree (NEXUS format): %s_aLRT.tre\n"%(args.output,args.output))
		sys.stdout.flush()
	return

#----------------------------------------------------------------------------------------#

def print_nni_tree(clade,trees2print,TRANSLATE,count_clades):
	clade=str(clade)
	head="#NEXUS\n[clade: %s]\nbegin trees;\ntranslate\n"%(clade)
	body=""
	for terminal in TRANSLATE:
		body+=" %s %s,\n"%(terminal,TRANSLATE[terminal])
	nexus="%s%s;\n"%(head,body[:len(body)-2])
	count_nni=0
	for nni_tree in trees2print:
		count_nni+=1
		nexus+="tree NNI%d_%d = %s;\n"%(count_clades,count_nni,nni_tree)
	nexus+="end;\n"
	filename="%s_aLRT_%d.tre"%(args.output,count_clades)
	handle=open(filename,"w")
	handle.write(nexus)
	handle.close()
	return filename

#----------------------------------------------------------------------------------------#

def runConf(confname):
	commandline="%s %s/%s > /dev/null 2>&1"%(args.garli,path,confname)
	subprocess.check_call(commandline,shell=True)
	return

#----------------------------------------------------------------------------------------#

def calcaLRT(clade_dic,best):
	alrt_dic={}
	for i in clade_dic:
		try:
			file="%s_%d_opt.best.all.tre"%(args.output,i)
			handle=open(file,"rU")
			tree=handle.read()
			handle.close()
		except:
			sys.stderr.write("> ERROR: could not read file %s\n"%(file))
			sys.stderr.flush()
			exit()
		scores=[float(x) for x in re.compile("\[!\s*garliscore\s*([^\]]+)",re.I).findall(tree)]
		alrt_dic[i]=best-max(scores)
	return alrt_dic

#----------------------------------------------------------------------------------------#

def reportaLRT(alrt_dic,clade_dic,clades,TRANSLATE):
	from math import exp
	tsv="Clade no.\tClade\taLRT value\texp(aLRT)\n"
	for i in clade_dic:
		tsv+="%s\t%s\t%s\t%f\n"%(str(i),str(clade_dic[i]),str(alrt_dic[i]),exp(alrt_dic[i]))
	tsv+="# KEY\tTRANSLATE\n"
	for i in TRANSLATE:
		tsv+="# %s\t%s\n"%(i,TRANSLATE[i])
	tree=ParseTrees.parenthetic(clades)[2:-2]
	count=0
	while "(" in tree:
		count+=1
		if count==10:
			exit()
		clades=re.compile("(\([^()]+\))").findall(tree)
		if not clades:
			break
		else:
			for clade in clades:
				terminals=re.sub(":[^(),;]+","",clade)
				terminals=re.sub("[\'\"\s]","",terminals)
				terminals=re.compile("[^,;\(\)\[\]]+").findall(terminals)
				for key in clade_dic:
					if(set(clade_dic[key])==set(terminals)):
						head=clade.replace("(","[")
						head=head.replace(")","]")
						tail=float(alrt_dic[key])
						tree=tree.replace(clade,"%s:%f"%(head,tail))
						del clade_dic[key]
						del alrt_dic[key]
						break
	tree="(%s);"%(tree)
	tree=tree.replace("[","(")
	tree=tree.replace("]",")")
	nexus="#NEXUS\n\nbegin trees;\n"
	for i in sorted(TRANSLATE):
		nexus+=" %s %s,\n"%(str(i),str(TRANSLATE[i]))
	nexus="%s;\ntree aLRT = %s\nend;\n"%(nexus[:-2],tree)
	return tsv,nexus

##########################################################################################

if(args.alrt):
	"""
	Take the first tree from the input nexus file.
	Create NNI for all the selected clades.
	Report the natural logarithm of the likelihood of the best trees found this way.
	"""
	main_alrt()
	exit()

"""
Start with general operations to calculate LLD values.
If --boostrap, run Boostrap searchers next.
If --alrt, run aLRT support in the end.
"""
configure()
if(args.bootstrapreps):
	"""
	Perform bootstrap analysis in Garli.
	"""
	bootstrap()
exit()

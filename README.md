[toc]

# Support4ML version 20160601

## AVAILABILITY

The "Support 4 ML" ( `s4ml`)  project is abvailable at GitLab:https://gitlab.com/MachadoDJ/s4ml.

## LICENSE

- Copyright (C) 2016 - Denis Jacob Machado
- GNU General Public License version 3.0

## AUTHOR
- Name: Denis Jacob Machado, Ph.D.
- Email: machadodj [at] alumni [dot] usp [dot] br

------------------------------------------------------------------------------------------

## DEPENDENCIES

1. Originally designed for Unix-like systems, but should work in other systems with none or minimal changes.
2. Python v3.* (should also work with Python v2.*)
3. A working local serial executable of Garli (v2.01)
4. A working local serial executable of sumtrees.py (v4.0)
5. DendroPy v4.0.3
6. GarliPy and Ybyra (packages comes with Support4ML)

### NOTE

Versions of programs not listed above have not been tested.

------------------------------------------------------------------------------------------

## INPUT
1. **Trees in Newick format (best tree from Garli):** Only the best tree of the file will be read. Node labels and branch lengths will be ignored if present.
2. **Template configuration file:** Fields that will be modified: `ofprefix` and `constraintfile`. Other fields that can be edit with argument options: searchreps, stopgen and stoptime. Edit other fields as wanted.



## OUTPUT
1. Reverse-constraint trees for Garli.
2. Reverse-constraint configuration files for Garli.
3. Nexus tree with log-likelihood difference (LLD) support values.

### Optional:

4. "Combined" Nexus tree with likelihood rations (LR) and bootstrap values.
5. Table in tab separated values with all LLD, LR and bootstrap values.

### Description of the output files

| Suffix           | Description                                         |
| ---------------- | --------------------------------------------------- |
| `boot.conf`      | Configuration files for Bootstrap analysis in Garli |
| `.conf`          | Configuration files for tree search in Garli        |
| `constraint.tre` | Constraint files for Garli                          |
| `_combined.phy`  | Outoput cladogram in Phyllip format                 |
| `_support.tre`   | Trees with node numbers and support values          |
| `_support.tsv`   | Support values in tab separated values              |

------------------------------------------------------------------------------------------

## USAGE

```
usage: support4ml [-h] [-b BOOTSTRAPREPS] [-c CONFIGURATION] [-g GARLI] [-o OUTPUT]
                  [-p PROCESSES] [-s SUMTREES] [-t TREE] [-v] [-V] [--burnin BURNIN]
                  [--clean] [--clades CLADES] [--frequency FREQUENCY]
                  [--searchreps SEARCHREPS] [--stopgen STOPGEN] [--stoptime STOPTIME]

optional arguments:
  -h, --help            show this help message and exit
  -b BOOTSTRAPREPS, --bootstrapreps BOOTSTRAPREPS
                        The number of bootstrap replicas to perform per
                        process (0 to infinity)
  -c CONFIGURATION, --configuration CONFIGURATION
                        Configuration file used during your tree search in
                        Garli (default = garli.conf)
  -g GARLI, --garli GARLI
                        Path to your serial Garli executable (default = garli)
  -o OUTPUT, --output OUTPUT
                        Preffix of the output constraints file (default =
                        constraint)
  -p PROCESSES, --processes PROCESSES
                        maximum processes to run simultaneously (default =
                        max. number of cpus)
  -s SUMTREES, --sumtrees SUMTREES
                        Path to sumtrees.py (default = sumtrees.py)
  -t TREE, --tree TREE  A tree file in Newich format (default =
                        search.best.tre)
  -v, --verbose         Increase output verbosity
  -V, --version         Print version number and quit
  --burnin BURNIN       The burn-in value for sumtrees (default=200)
  --clean               Remove least important output files
  --clades CLADES       Constraint file with selected clades
  --frequency FREQUENCY
                        The percentage value for consensus tree (0 to 1.0;
                        default=0.95)
  --searchreps SEARCHREPS
                        The number of independent search replicates (1 to
                        infinity)
  --stopgen STOPGEN     The maximum number of generations to run
  --stoptime STOPTIME   The maximum number of seconds for the run to continue
```



### Notes

1. If the number of processors (`-p`) is not given, will use the maximum number of processors found.
2. The argument option `-b` or `--bootstrapreps` sets the number of bootstrap reps to be performed per processor. If left 0 or blank, will not run boostrap analyses.
3. The argument options `--searchreps`, `--stopgen` and `--stoptime` will change the searchrep, stopgen and stoptime values (respectively) in the template configuration file. If left blank, will keep exactly the same value provided in the template.

### Advanced options

To select only a few clades for support calculation, you must provide (with argument `--clades`), a input text file with one clade per lines.

Clades are indicated by a list of comma separated terminal names, and the terminals names must match precisely those of the input trees and matrix.

If you provide a clades file, only the clades listed in there will be used for log-likelihood difference calculations.

------------------------------------------------------------------------------------------

## QUICK TUTORIAL

### Files needed

- `matrix.nex`
- `SEARCH.best.tre`
- The Support4ML program (`support4ml.py`)

### Command line

```
$ python support4ml.py -t SEARCH.best.tre -c garli.conf -o TEST -v -p 4 --stopgen 10 --bootstrapreps 10 --burnin 2
```



### What the command line does

1. Calculate LLD and LR for all the nodes
2. Use garli.conf as configuration template
3. Use TEST as prefix for the output file names
4. Increase output verbosity
5. Uses up to 4 processors
6. Set `stopgen` to 10
7. After calculating the LLD and LR, run 10 reps. of boostrap using 4 processes
8. Executes `sumtrees.py` with a burnin value of 2

### Expected results

- Support in tab separated values will be saved into into `TEST_support.tsv`
- Tree with node numbers and support values will be saved into into `EST_support.tre`
- Tree with LR and bootstrap will be saved into into `TEST_combined.tre`
- Output tree in Phyllip format will be saved into into `TEST_combined.phy`
- Table in tab separated values will be saved into `TEST_combined.tsv`

------------------------------------------------------------------------------------------

## SUPPORT CALCULATIONS

Garli uses the natural logarithm of the likelihood (lnL) on it's scores. Therefore, LLD is calculated by simply subtracting S1 and S2, where S1 is the highest Garli Score and S2 is the lowest Garli Score. LR is the AntiLog of LLD, or e (the Euler's number) to the power of LLD.

------------------------------------------------------------------------------------------

## FINANCIAL SUPPORT

Financial support from Fundação de Amparo à Pesquisa do Estado de São Paulo (FAPESP), grants 2012/10.000-5 and 2013/05958-8.